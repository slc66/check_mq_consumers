# check_mq_consumers

## Description
This is a Nagios NRPE ActiveMQ check written in Python 3.

The aim here is to detect queues that are loaded but not consumed.

Configuration file (.conf):
- URL
- Authentification
- Ignored Queues

Required modules:
- requests
    HTTP library, written in Python, for human beings
- lxml
    XML processing library combining libxml2/libxslt
    with the ElementTree API



## Installation
Just download the python code. Place the configuration file next to it. Then adapt the configuration to your needs.



## Usage

Add this program as an NRPE probe to your Nagios/Centreon configuration.




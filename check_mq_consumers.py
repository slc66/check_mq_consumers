#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" ====================================================================
    Nagios NRPE ActiveMQ check
    stephane@slc66.net
    27/05/2021 initial version
    --------------------------------------------------------------------
    The aim here is to detect queues that are loaded but not consumed.

    Configuration file (.conf):
    - URL
    - Authentification
    - Ignored Queues

    Required modules:
    - requests
        HTTP library, written in Python, for human beings
    - lxml
        XML processing library combining libxml2/libxslt
        with the ElementTree API

    ====================================================================
"""

import sys
import os
import json
import requests
from lxml import etree

# ======================================================================
# Globales
# ======================================================================
STATUS_OK = 'OK'
STATUS_WARNING = 'WARNING'
STATUS_CRITICAL = 'CRITICAL'
STATUS_UNKNOWN = 'UNKNOWN'

status = STATUS_UNKNOWN
exit_status = {STATUS_OK: 0, STATUS_WARNING: 1,
          STATUS_CRITICAL: 2, STATUS_UNKNOWN: 3}
exit_msg = ''


# ======================================================================
# Functions
# ======================================================================
def set_status(level: str, msg=''):
    """ Assign exit status and message
    """
    global exit_status
    global exit_msg
    global status

    status = level

    if msg:
        exit_msg += msg


# ----------------------------------------------------------------------
def load_config():
    """ Load configuration from JSON file.
        Filename is equal to main program file with .conf suffix.
    """

    # Assign default config
    config = []

    # Assign filename
    fconf = sys.argv[0][:-3] + '.conf'

    if os.path.isfile(fconf):
        # Read file content
        with open(fconf, 'r') as fjson:
            content = fjson.read()

        try:
            # Parse configuration from JSON
            loaded_config = json.loads(content)

        except ValueError as error:
            set_status(STATUS_CRITICAL,
                       'Erreur de decodage du fichier de configuration ' +
                       f'{fconf} :\n{error}')

        if loaded_config.keys() >= {'check'}:
            # check is defined
            if not loaded_config['check'].keys() >= {'url', 'auth'}:
                # ERROR : URL or Authentification not defined
                set_status(STATUS_CRITICAL,
                           'Clé url ou auth non définie')

            else:
                # Configuration is Ok
                config = loaded_config

        else:
            # ERROR: key check not defined
            set_status(STATUS_CRITICAL,
                       'Clé check non définie')

    else:
        # ERROR: Configuration file do not exist
        set_status(STATUS_CRITICAL,
                   f'Fichier {fconf} non trouvé')

    return config


# ----------------------------------------------------------------------
def exit_program():
    """ Print message before exit program
    """
    global status
    global exit_status
    global exit_msg

    if (status == STATUS_OK):
        print(status)

    else:
        print(status + ': ' + exit_msg)

    sys.exit(exit_status[status])


# ======================================================================
# Main
# ======================================================================
if __name__ == '__main__':
    # Load configuration
    config = load_config()

    if not config:
        # An error occured while loading configuration, exit
        set_status(STATUS_CRITICAL)
        exit_program()

    try:
        # Get ActiveMQ queues informations
        queues = requests.get(config['check']['url'],
                              auth=tuple(config['check']['auth']))

        # Raise exception if status error
        queues.raise_for_status()

    except requests.exceptions.RequestException as error:
        # Exit on error
        set_status(STATUS_CRITICAL,
                   f'ERREUR sur requete HTTP\n{error}')
        exit_program()

    if queues.status_code == 200:

        # Parsing the result
        try:
            root = etree.fromstring(queues.text)

        except ValueError as error:
            set_status(STATUS_CRITICAL,
                       'ERREUR lors du traitement du retour ActiveMQ\n' +
                       f'{error}')
            exit_program()

        # Set status to Ok
        set_status(STATUS_OK)

        # For each queue
        for queue in root:

            # Get the name
            name = queue.get('name')
            size = 0
            consumerCount = 0

            if name not in config['ignore']:
                # Get Size and Cosumers number in the 'stats' node
                stats = queue.find('stats')

                if stats is not None:
                    size = int(stats.get('size'))
                    consumerCount = int(stats.get('consumerCount'))

                if size > 0 and consumerCount <= 0:
                    # ERROR : The queue has no consumer
                    set_status(STATUS_CRITICAL, f'{name} ({size}) ')

# All is Ok
exit_program()
